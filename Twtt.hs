----------------------------------------------------------------------------
-- |
-- Module      :  Twtt
-- Copyright   :  (c) Ruben Pollan 2011
-- License     :  GPLv3+ (see LICENSE)
--
-- Maintainer  :  meskio@sindominio.net
-- Stability   :  unstable
-- Portability :  portable
--
--
-----------------------------------------------------------------------------

module Main where

import System.Environment

import Twtt.UI
import Twtt.Command
import Twtt.Config

main = do
    homeDir <- getEnv "HOME"
    mConf <- loadConfig $ homeDir ++ "/.twttrc"
    (token, secret) <- case mConf of
                           Right x -> return ((confValue x "token"), (confValue x "secret"))
                           Left _ -> fail ""
    loop $ getCmd token secret
