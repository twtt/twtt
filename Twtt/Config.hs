----------------------------------------------------------------------------
-- |
-- Module      :  Twtt.Config
-- Copyright   :  (c) Ruben Pollan 2011
-- License     :  GPLv3+ (see LICENSE)
--
-- Maintainer  :  meskio@sindominio.net
-- Stability   :  unstable
-- Portability :  portable
--
--
-----------------------------------------------------------------------------

module Twtt.Config (
   Config,

   loadConfig,
   confValue
  )where

import Data.ConfigFile
import Data.Either.Utils
import Control.Monad.Error

-- | Config information
--
--   Initialized with 'loadConfig' and accessed  with 'confValue'
type Config = ConfigParser

-- | Load config file
loadConfig :: String                -- ^ Path of the config file
           -> IO (Either CPError Config)  -- ^ The config information
loadConfig path = readfile emptyCP path

-- | get a value from the config
confValue :: Config                -- ^ Config
          -> String                -- ^ entry name
          -> String  -- ^ value
confValue cp entry = forceEither $ get cp "DEFAULT" entry
