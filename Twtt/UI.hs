----------------------------------------------------------------------------
-- |
-- Module      :  Twtt.UI
-- Copyright   :  (c) Ruben Pollan 2011
-- License     :  GPLv3+ (see LICENSE)
--
-- Maintainer  :  meskio@sindominio.net
-- Stability   :  unstable
-- Portability :  portable
--
--
-----------------------------------------------------------------------------

module Twtt.UI (
  loop
  )where

import Graphics.Vty.Widgets.All
import Data.Maybe

import Twtt.Command

execCmd :: Cmd -> String -> IO ()
execCmd cmd str = (fromJust $ command cmd c) param
    where (c, param) = foldl (\(a, b) x -> if b == " "
                                             then if x == ' '
                                                    then (a, "") 
                                                    else (a++[x], b)
                                             else (a, b++[x]))
                       ("", " ") str

loop :: Cmd -> IO ()
loop cmd = do
    e <- editWidget
    ui <- centered e

    fg <- newFocusGroup
    addToFocusGroup fg e

    c <- newCollection
    _ <- addToCollection c ui fg

    e `onActivate` \this -> getEditText this >>= execCmd cmd

    runUi c defaultContext
