----------------------------------------------------------------------------
-- |
-- Module      :  Twtt.Command
-- Copyright   :  (c) Ruben Pollan 2011
-- License     :  GPLv3+ (see LICENSE)
--
-- Maintainer  :  meskio@sindominio.net
-- Stability   :  unstable
-- Portability :  portable
--
--
-----------------------------------------------------------------------------

module Twtt.Command (
  Cmd,

  getCmd,
  command
  )where

import Web.Twitter
import Web.Twitter.Monad
import Web.Twitter.Fetch

import Data.List

-- | Command list of twitter commands
--
--   [(name, fuction)]
type Cmd = [(String, (String -> IO ()))]

app = App "5WkzENpTFtBJyo7eEPEtsQ" "mLy935QvZpTjXpQ9zwWo7FYBvaECiflg3Qa8flhgew"

-- | Generate the command list from the user token
getCmd :: String -> String -> Cmd
getCmd token secret = [("update", run update)]
    where user = initUser app token secret []
          run cmd = (\s -> runTM user $ cmd s Nothing)

-- | Return the command function
command :: Cmd -> String -> Maybe (String -> IO ())
command cmd name = case find (\(c, _) -> c == name) cmd of
                       Nothing -> Nothing
                       Just (_, f) -> Just f

